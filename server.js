const http = require("http")
const fs = require("fs")
const { v4: uuid4 } = require("uuid")
const status = require("statuses")
// const path = require("path")


const server = http.createServer((request, response) => {
    const url = request.url
    if (url === '/') {
        response.end("HOME PAGE")
    }



    if (url === '/html') {
        fs.readFile('index.html', (error, content) => {
            if (error) throw error
            response.writeHead(200, { 'Content_Type': 'text/html' })
            response.end(content)
        })
    }

    if (url === '/json') {
        fs.readFile('data.json', (error, data) => {
            if (error) throw error
            response.writeHead(200, { 'Content_Type': 'application/json' })
            response.end(JSON.stringify(JSON.parse(data)))
        })

    }

    if (url === '/uuid') {
        const val = uuid4()
        response.writeHead(200, { 'Content_Type': 'application/json' })
        response.end(JSON.stringify({ "uuid": val }))
    }

    if (url.match(/(status)/)) {
        const status_code = url.slice(8, 12)
        const code_response = status(status_code)
        const message = `Response of ${status_code} is ${code_response}`
        response.write(message)
        response.end()
    }

    if (url.match(/(delay)/)) {
        const delay = url.slice(7)
        const message = `Response of 200 is ${status(200)}`
        setTimeout(() => {
            response.write(message)
            response.end()
        }, delay * 1000)
    }

})


server.listen(8000, "127.0.0.1", () => {
    console.log("Listening to port number 8000")
})